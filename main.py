import re
import csv
import json
import webapp2
from datetime import datetime

from google.appengine.api import users
from google.appengine.ext import blobstore
from google.appengine.ext import ndb
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext.webapp import template
from models import SmsData

class UploadFormHandler(webapp2.RequestHandler):
    def get(self):
	params = {}

        upload_url = blobstore.create_upload_url('/upload_file')
	params['upload_url'] = upload_url
	self.response.out.write(template.render('index.html', params))

class UploadFileHandler(blobstore_handlers.BlobstoreUploadHandler):
    def post(self):
        upload = self.get_uploads()[0]
	blob_reader = blobstore.BlobReader(upload.key())
	blob_reader_data = blob_reader.read()
    	datareader = json.loads(blob_reader_data)

	msg = datareader['messages']
	if msg is not None:
	    self.process_sms(msg)
	    return self.redirect('/dashboard')

    def process_sms(self, msg):
	for i in xrange(len(msg)):
	    txt = msg[i]['text'].lower().replace(',','')
	    amt = re.findall('\d+\.\d+', txt)
	    text_split = txt.split()
	    for text in text_split:
		if len(amt) > 0 and text.find('xxxx') != -1:
	    	    cc_number = 'xxxx'+text[-4:]
	    	    sender_id = msg[i]['number']
		    amount = float(amt[0])
	    	    trs_date = datetime.fromtimestamp(int(msg[i]['timestamp'])/1000)
		    sms_date = datetime.fromtimestamp(int(msg[i]['timestamp'])/1000)
	    	    sms_data = SmsData(
			sender_id=sender_id, credit_card_number=cc_number, amount=amount,
			transaction_datetime=trs_date, sms_receive_datetime=sms_date)
	    	    key = sms_data.put()
		    if key is not None:
			print 'Successfully Added'
		else:
		    continue

class DashboardHandler(webapp2.RequestHandler):
    def get(self):
	params = {}

	smsdata = SmsData.query().order(SmsData.sms_receive_datetime).fetch()
	params['smsdata'] = smsdata
	self.response.out.write(template.render('dashboard.html', params))
	

class DeleteSmsData(webapp2.RequestHandler):
    def get(self):

	smsdata = SmsData.query().fetch()
	for i in smsdata:
	    if i.key:
		i.key.delete()
	self.redirect('/')
	

app = webapp2.WSGIApplication([
    ('/', UploadFormHandler),
    ('/upload_file', UploadFileHandler),
    ('/dashboard', DashboardHandler),
    ('/delete_sms', DeleteSmsData),
], debug=True)
