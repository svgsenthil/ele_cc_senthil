from google.appengine.ext import ndb

class SmsData(ndb.Model):
    sender_id = ndb.StringProperty()
    credit_card_number = ndb.StringProperty()
    amount = ndb.FloatProperty()
    transaction_datetime = ndb.DateTimeProperty()
    sms_receive_datetime = ndb.DateTimeProperty()
